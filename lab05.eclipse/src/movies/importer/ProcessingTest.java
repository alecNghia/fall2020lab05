package movies.importer;

import java.io.IOException;

public class ProcessingTest {
	public static void main(String[] args) throws IOException {
		String source = "S:\\CompSci\\Fall 2020\\Java III\\lab05_test_subjects\\testInput";
		String destination = "S:\\CompSci\\Fall 2020\\Java III\\lab05_test_subjects\\testOutput";
		LowercaseProcessor lower = new LowercaseProcessor(source, destination);
		lower.execute();
		
		String source1 = "S:\\CompSci\\Fall 2020\\Java III\\lab05_test_subjects\\testOutput";
		String destination1 = "S:\\CompSci\\Fall 2020\\Java III\\lab05_test_subjects\\testOutput2";
		
		RemoveDuplicate remove = new RemoveDuplicate(source1, destination1);
		remove.execute();
	}
}
