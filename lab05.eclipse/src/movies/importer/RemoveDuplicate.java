package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicate extends Processor{

	public RemoveDuplicate(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> inputArr) {
		ArrayList<String> arr = new ArrayList<String>();
		String strIn;
		
		for(int i = 0; i < inputArr.size(); i++) {
			strIn = inputArr.get(i);
			
			if(!arr.contains(strIn)) {
				arr.add(inputArr.get(i));
			}
		}
		return arr;
	}
}
